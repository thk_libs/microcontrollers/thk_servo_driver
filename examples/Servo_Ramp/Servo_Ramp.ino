// Dieses Beispiel veranschaulicht die Verwendung der ServoRampDriver-Klasse
// Dazu wird eine Servo-Instanz und eine ServoRampDriver-Instanz gebildet.
// ServoRampDriver steuert den Servo und ServoRampDriver bekommt 
// Geschwindigkeitsvorgaben und Winkelvorgaben im loop().

#define SERIALOUT_FOR_PLOTTER

#include "thk_servo_ramp_driver.h"

Servo servo;                                                                           
ServoRampDriver servot(servo, 100);     // Parameter: Servoinstanz, Winkelgeschwindigkeit                                               

void setup()
{
  Serial.begin(115200);
  delay(1000);
  servo.attach(9);
  servot.set_port(&Serial);
  servot.set_angle(170);
  Serial.println("ServoTravelTest: Über seriellen Plotter anzeigen!");
}

void loop()
{
  static int16_t i, a=10, v=150;
  const static int16_t ANGLE[2] = {10, 170};
  const static int16_t VELOCITY[2] = {150, 50};
  static uint32_t t;

  if (millis() - t > 5000)
  {
    t = millis();
    a = ANGLE[i % 2];
    v = VELOCITY[i % 2];
    i++;
  }
  servot.go(a, v);
}