
// Dieses Beispiel veranschaulicht die Verwendung der ServoStep-Klasse.
// Dieses Beispiel erzeugt Vorgabewinkel für einen Servo. 
// In einem bestimmten Zeitabstand wechselt der Winkel zwischen zwei Werten.
// Somit fährt der Servo zwischen zwei Winkelstellungen hin und her.
// Zudem wird die Winkelgeschwindigkeit verändert.
// -> Im seriellen Plotter kann die Steuerung nachvollzogen werden.
// -> Der FACTOR kann zwischen 1 und 10 verändert werden -> im seriellen Plotter nachvollziehen!

#define SERIALOUT_FOR_PLOTTER

#include "thk_servo_driver.h"

#define SAMPLETIME 25
#define SERVOPIN 9
#define FACTOR 1 // <---- hier Änderungen  die Auswirkungen im ser. Plotter anschauen


Servo servo;                                          // Servo-Instanz
int16_t angle;                                        // Speicherung des Servos-Winkels
ServoDriver sstep(servo, SAMPLETIME, &angle, FACTOR);   // Die Instanz zur Steuerung der Servobewegung
                                                          //   (Servo, Samplezeit in ms, Zeiger auf Winkelwert, Entschleunigungsfaktor)

void setup()
{
  Serial.begin(115200);
  delay(1000);
  servo.attach(SERVOPIN);
  sstep.set_port(&Serial);
  Serial.println("Maxwinkel Zielwinkel Winkelstellung Geschwindigkeit Minwinkel");
}

void loop()
{
  static int16_t i, a, v;
  static int16_t ANGLE[2] = {10, 170};
  static int16_t VELOCITY[2] = {150, 50};
  static uint32_t t;

  if (millis() - t > 2000)
  {
    t = millis();
    a = ANGLE[i % 2];
    v = VELOCITY[i % 2];
    i++;
  }
  sstep.go(a, v);
}