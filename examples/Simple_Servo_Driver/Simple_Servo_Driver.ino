// Dieses Beispiel veranschaulicht die Verwendung der ServoStep-Klasse.
// Dafür werden Vorgabewinkel für einen Servo erzeugt. 
// In einem bestimmten Zeitabstand wechselt der Winkel zwischen zwei Werten.
// Somit fährt der Servo zwischen zwei Winkelstellungen hin und her.
// -> Im seriellen Plotter kann die Steuerung nachvollzogen werden.
//    -> Bitte Änderungen von ANGLEVELOCITY experimentell nachvollziehen

#define SERIALOUT_FOR_PLOTTER

#include "thk_servo_driver.h"

#define SERVOPIN 9
#define ANGLEVELOCITY 80


Servo servo;                                  // Servo-Instanz
int16_t angle;                                // Speicherung des Servos-Winkels
ServoDriver sstep(servo, &angle);               // Die Instanz zur Steuerung der Servobewegung
                                                //   (Servo, Zeiger auf Winkelwert)

void setup()
{
  Serial.begin(115200);
  delay(1000);
  servo.attach(SERVOPIN);
  sstep.set_port(&Serial);
}

void loop()
{
  static int16_t i, a;
  const static int16_t A[2] = {10, 170};
  static uint32_t t;

  if (millis() - t > 3000)
  {
    t = millis();
    a = A[i % 2];
    i++;
  }
  sstep.go(a, ANGLEVELOCITY);
}