#line 2 "UnitTest_servo_driver.ino"

#include <AUnit.h>
#include "thk_servo_driver.h"

Servo servo;
int16_t angle;
ServoDriver sstep(servo, &angle);


test(01_calc_angle_velocity) // 10Grad in 100 [ms] = 1 [Centigrad]/s
{
  //Achtung Angabe in: [Centigrad], [Centigrad], [ms]
  int16_t w = sstep.calc_angle_velocity(0, 100, 100);
  assertEqual(w, 1);
}

test(02_calc_angle_velocity) // -10Grad in 100 [ms] = -1 [Centigrad]/s
{
  //Achtung Angabe in: [Centigrad], [Centigrad], [ms]
  int16_t w = sstep.calc_angle_velocity(100, 0, 100);
  assertEqual(w, -1);
}

test(03_calc_angle_velocity) // 1Grad in 100 [ms] = 0.1 [Centigrad]/s = 0 (Integer)
{
  //Achtung Angabe in: [Centigrad], [Centigrad], [ms]
  int16_t w = sstep.calc_angle_velocity(0, 10, 100);
  assertEqual(w, 0);
}

test(04_calc_angle_velocity) // 1Grad in 11 [ms] = 0.91 [Centigrad]/s = 0 (Integer)
{
  //Achtung Angabe in: [Centigrad], [Centigrad], [ms]
  int16_t w = sstep.calc_angle_velocity(0, 10, 11);
  assertEqual(w, 0);
}

test(05_set_angle_velocity)
{
  // Achtung Angabe in: [Centigrad], [Centigrad], [Grad/s], [Grad/s]
  int16_t w = sstep.set_angle_velocity(0, 10, 10, 1); // 1 Grad fahren mit 10/s oder 1/s
  assertEqual(w, 1);
}

test(06_set_angle_velocity)
{
  // Achtung Angabe in: [Centigrad], [Centigrad], [Grad/s], [Grad/s]
  int16_t w = sstep.set_angle_velocity(0, 10, 1, 10); // 1 Grad fahren mit 1/s oder 10/s? 
  assertEqual(w, 1);
}

test(07_set_angle_velocity)
{
  // Achtung Angabe in: [Centigrad], [Centigrad], [Grad/s], [Grad/s]
  int16_t w = sstep.set_angle_velocity(10, 0, 1, -10); // -1 Grad fahren mit -10/s oder -1/s (Eingabe der Absolutgeschwindigkeit!)
  assertEqual(w, -1);
}

test(08_go)
{
  // Test von 0 [Centigrad] nach 100 [Grad] mit Geschwindigkeit 100/[s] und einer Samplezeit von 10 [ms] 
  //   sollte 100 [Centigrad] ergeben
  //
  // Achtung Angabe in: von [Centigrad], nach [Grad], [Grad/s], [ms]
  int16_t angle = 0;
  sstep.go(&angle, 100, 100, 10);
  assertEqual(angle, 100);
}

test(09_go)
{
  // Test von 10000 [Centigrad] nach 0 [Grad] mit Geschwindigkeit 100/[s] und einer Samplezeit von 10 [ms] 
  //   sollte 10000-100 = 9900 [Centigrad] ergeben
  //
  // Achtung Angabe in: von [Centigrad], nach [Grad], [Grad/s], [ms]
  int16_t angle = 10000; //[Centigrad]
  sstep.go(&angle, 0, 100, 10);
  assertEqual(angle, 10000-100);
}
void setup()
{
  Serial.begin(115200);
}

void loop()
{
  aunit::TestRunner::run();
}