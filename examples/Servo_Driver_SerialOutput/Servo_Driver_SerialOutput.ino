// Dieses Beispiel veranschaulicht die Verwendung der ServoStep-Klasse.
// Dafür werden Vorgabewinkel für einen Servo erzeugt. 
// In einem bestimmten Zeitabstand wechselt der Winkel zwischen zwei Werten.
// Somit fährt der Servo zwischen zwei Winkelstellungen hin und her.
// -> Der FACTOR kann zwischen 1 und 10 verändert werden 
//        -> im seriellen Plotter nachvollziehen!

#define SERIALOUT_FOR_PLOTTER

#include "thk_servo_driver.h"

#define SERVOPIN 9
#define ANGLEVELOCITY 200
#define FACTOR 10    // <---- hier Änderungen  die Auswirkungen im ser. Plotter anschauen


Servo servo;                                          // Servo-Instanz
int16_t angle;                                        // Speicherung des Servos-Winkels
ServoDriver sstep(servo, &angle, FACTOR);               // Die Instanz zur Steuerung der Servobewegung
                                                          //   (Servo, Samplezeit in ms, Zeiger auf Winkelwert, Entschleunigungsfaktor)

void setup()
{
  Serial.begin(115200);
  delay(1000);
  servo.attach(SERVOPIN);
  sstep.set_port(&Serial);
  Serial.println("Maxwinkel Zielwinkel Winkelstellung Geschwindigkeit Minwinkel");
}

void loop()
{
  static int16_t i, a;
  const static int16_t A[2] = {10, 170};
  static uint32_t t;

  if (millis() - t > 2000)
  {
    t = millis();
    a = A[i % 2];
    i++;
  }
  sstep.go(a, ANGLEVELOCITY);
}