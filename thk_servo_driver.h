#ifndef SERVO_STEP_H
#define SERVO_STEP_H

#include <Arduino.h>
#include <Servo.h>

#define TOLERANCE_ANGLE 100 // erlaubte Differenz SOLL-IST-Winkel [Centigrad]

class ServoDriver
{
private:
  Servo &servo;              // Servo wird in übergeordner Instanz gespeichert
  const uint16_t sampleTime; // [ms] Zeit in ms für Taktung der Servofahrten
  int16_t *p_ang;            // [CentiGrad] Aktuelle Winkelstellung; Wird in übergeordneter Instanz gespeichert
  uint32_t t;                // [ms] lokal verwendete Zeit für Taktungssteuerung
  int16_t K;                 // [] Faktor für Geschwindigkeitsanpassung vor dem Ziel 
                             //        1=Servo wird langsamer falls zur Erreichen der Zielstellung noch Zeit vorhanden ist
                             //       10=Servo fährt mit angegebener Geschwindigkeit auf die Zielstellung
  Stream *p_port;            // Pointer auf Serialobjekt für Ausgaben

public:
  ServoDriver(Servo &_servo, int16_t *_p_ang, const uint16_t _sampleTime=10, int16_t _K=10)
      : servo(_servo), sampleTime(_sampleTime), K(_K){p_ang=_p_ang;};



  // Seriellen Port für Ausgaben übergeben
  void set_port(Stream *_p_port)
  {
    p_port = _p_port;
  }




  // Aufgabe: Fahre den nächsten Step, falls Samplezeit "abgelaufen"
  // Verwendung: Zyklischer Aufruf im loop()
  //
  // Parameter: Winkel in [Grad], Winkelgeschwindigkeit in [Grad/s]
  // Rückgabe: TRUE=falls <0.1 Grad (TOLERANCE_ANGLE) vom Ziel entfernt
  bool go(const int16_t &ang_end, const int16_t &ang_vel) // [Grad], [Grad/s]
  {
    if ((millis() - t) > (uint32_t)sampleTime)
    {
      t = millis();
      go(p_ang, ang_end, ang_vel, sampleTime);
      servo.write(*p_ang / 100);
      #ifdef SERIALOUT_FOR_PLOTTER
        // Ausgabe für seriellen Plotter:
        print_plotter(*p_ang / 100, ang_end, ang_vel);
      #else
        // Ausgabe des Zielwinkels für den Step:
        p_port->println(*p_ang / 100);
      #endif
    }
    
    return abs(abs(ang_end * 100) - abs(*p_ang)) < TOLERANCE_ANGLE; // <100 [Centigrad]
  }



  // Berechnung der nötigen Winkelgeschwindigkeit, um von einem zum nächsten Punkt zu fahren
  //
  // Parameter: Winkel in [CentiGrad], Zeit in [ms]
  // Rückgabe: Winkelgeschwindigkeit in [CentiGrad/ms]=10*[Grad/s]
  int16_t calc_angle_velocity(const int16_t &a_beg, const int16_t &a_end, const uint16_t &sampletime)
  {
    return (a_end - a_beg) / (int16_t)sampletime;
  }



  // Entscheidung ob die Vorgabegeschwindigkeit gefahren wird, oder die berechnete Geschwindigkeit,
  // um zu vermeiden, dass der Servo in der Samplezeit zu weit fährt.
  //
  // Achtung: "a_vel" ist immer positiv, da Vorgabegeschwindigkeit.
  // Das Vorzeichen für "a_vel" wird über die Richtung bestimmt. "a_vel_calc" hat bereits ein Vorzeichen.
  //
  // Parameter: Winkel in [CentiGrad], Winkelgeschwindigkeit in [Grad/s]
  // Rückgabe: Winkelgeschwindigkeit in [CentiGrad/ms]=10*[Grad/s]
  int16_t set_angle_velocity(const int16_t &a_beg, const int16_t &a_end, const int16_t &a_vel, const int16_t &a_vel_calc)
  {
    return (a_end > a_beg) ? min(a_vel, a_vel_calc) : max(a_vel_calc, -a_vel);
  };



  // Berechne und Fahre den nächsten Step
  //
  // Parameter: Winkel in [CentiGrad], [Grad], Winkelgeschwindigkeit in [Grad/s], Zeit in [ms]
  // Rückgabe: Servostellwert in [Grad]
  int16_t go(int16_t *_p_ang, int16_t ang_end, int16_t ang_vel, const uint16_t &sampletime)
  {
    ang_end *= 100; // [Grad] -> [CentiGrad]

    int16_t ang_vel_calc = calc_angle_velocity(*_p_ang, ang_end, sampletime); // [CentiGrad/ms]=10*[Grad/s]

    ang_vel_calc *= K; // k*[10*Grad/s]

    ang_vel = set_angle_velocity(*_p_ang, ang_end, ang_vel, ang_vel_calc); // [CentiGrad/ms]=10*[Grad/s]

    *_p_ang += (ang_vel * (int16_t)sampletime) / 10; // Wert am Ursprungsort speichern in [CentiGrad]

    return *_p_ang / 100;
  };



  // Serielle Ausgabe der zu fahrenden Winkel und der Winkelgeschwindigkeit
  //
  // Umschalteoption der Ausgabe per Präprozessor-Direktive:
  //   SERIALOUT_FOR_PLOTTER : zeichnet einen Verlauf von: Zielwinkel, aktueller Winkel, Winkel-Geschwindigkeit
  //   #else                 : gibt nur den aktuellen Winkel aus
  void print_plotter(const int16_t &ang, const int16_t &ang_end, const int16_t &ang_vel)
  {
    if (p_port != NULL)
    {
      // Ausgabe des Zielwinkels, aktuellen Winkels, der Winkelgeschwindigkeit für den Step:
      p_port->print(180);
      p_port->print(F(" "));
      // p_port->print(F(" Ziel="));
      p_port->print(ang_end);
      p_port->print(F(" "));
      // p_port->print(F("Aktuell="));
      p_port->print(ang);
      p_port->print(F(" "));
      // p_port->print(F("Geschw="));
      p_port->print(ang_vel);
      p_port->print(F(" "));
      p_port->print(0);
      p_port->print(F(" "));
      p_port->println();
    }
  }
};

#endif
