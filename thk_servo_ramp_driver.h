#ifndef SERVO_STEPCHAIN_H
#define SERVO_STEPCHAIN_H

#include <Arduino.h>
#include <Servo.h>
#include "thk_servo_driver.h"

#define RAMP_VELOCITY_REDUCTION 50 // [%] Geschwindigkeit wird um x% reduziert
#define RAMP_WAY_PERCENTAGE 15     // [%] Anteil des Weges, der mit RAMP-Geschwindigkeit gefahren werden soll

enum StepState {RAMPUP, DRIVE, RAMPDOWN, STAY};

class ServoRampDriver
{
  private:
    Servo &servo;                              // Servo wird in übergeordner Instanz gespeichert
    const uint8_t sampleTimeServo = 50;         // [ms] Zeit in ms für Taktung der Servofahrten
    int16_t ang = 9000;                        // [CentiGrad] Aktueller Winkel: Wird in dieser Instanz gespeichert und an die "steps" durchgereicht
    int16_t ang_beg;                           // [Grad] Startwinkel
    int16_t ang_vel_ramp;                      // [Grad/s]
    int16_t ang_vel_drive;                     // [Grad/s]
    uint32_t t;                                // [ms] Zeit für Taktungssteuerung
    StepState sstate;                          // Servozustand
    ServoDriver* sstep;
    Stream* p_port;                            // Pointer auf Serialobjekt für Ausgaben

  public:
    ServoRampDriver(Servo &_servo, int16_t _ang_vel_drive): servo(_servo), ang_vel_drive(_ang_vel_drive)
    {
      sstate = STAY;
      ang_vel_ramp = ang_vel_drive * RAMP_VELOCITY_REDUCTION / 100;

      if (sstep == NULL)
        sstep = new ServoDriver(servo, &ang, sampleTimeServo);
    };

    void set_port(Stream* _p_port)
    {
      p_port = _p_port;
      sstep->set_port(_p_port);
    }

    void set_angle(int16_t _ang)
    {
      ang = _ang * 100;
    }

    bool  go(const int16_t& _ang_end, int16_t _ang_vel_drive)
    {
      ang_vel_drive = _ang_vel_drive;
      ang_vel_ramp = ang_vel_drive * RAMP_VELOCITY_REDUCTION / 100;
      return go(_ang_end);
    }

    bool  go(const int16_t& _ang_end)
    {
      bool result = false;
      static int16_t old_ang;
      static int16_t way;

      if (_ang_end != old_ang)
      {
        ang_beg = ang / 100;
        old_ang = _ang_end;
        way = _ang_end - ang_beg;
        sstate = RAMPUP;
      }

      switch (sstate)
      {
        case STAY:
          //sstep->go(ang, 0);
          break;
        case RAMPUP:
          if (sstep->go(ang_beg + way * RAMP_WAY_PERCENTAGE/100, ang_vel_ramp))
          {
            sstate = DRIVE;
          }
          break;
        case DRIVE:
          if (sstep->go(_ang_end - way * RAMP_WAY_PERCENTAGE/100, ang_vel_drive))
          {
            sstate = RAMPDOWN;
          }
          break;
        case RAMPDOWN:
          if (sstep->go(_ang_end, ang_vel_ramp))
          {
            sstate = STAY;
            result = true;
          }; break;

      };
      return result;
    }


};

#endif
