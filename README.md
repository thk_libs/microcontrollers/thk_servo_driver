# **ServoDriver**

Diese Klasse hat zusätzliche Funktionen für die Servosteuerung und soll die Funktionen der Servo.h-Library etwas ergänzen:
 * Ein ServoDriver kann eine Zielstellung mit einer definierten Geschwindigkeit erreichen.
 * Der Servo wird dann mit dieser Geschwindigkeit zu seiner Zielposition gefahren.
 * Kommt ein neuer Zielwinkel während einer noch andauernden Fahrt, so wird das neue Ziel direkt gültig und das alte Ziel verworfen.
 * Dazu wird der Zielwinkel als Referenz in ServoDriver verarbeitet. Das bedeutet alle Änderungen des Zielwinkels im aufrufenden Programm werden beachtet.
 * go() muss dazu im loop kontinuierlich gerufen werden.

 

## **Installation:**

Um diese Klassen verwenden zu können, muss dieses Repository geklont und in das Library-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung:**

Zur Verwendung siehe die Beispiele:
* Simple_Servo_Driver.ino
* Servo_Driver_SerialOut.ino
* Servo_Driver_VelocityChange.ino
* UnitTest_servo_driver.ino
<br />


## **Wokwi-Simulator:**
Die Dateien `diagram.json` und `wokwi.toml` wurden für das Beispiel Simple_Servo_Driver.ino erstellt. Zum Starten der Simulation den Wokwi-Simulator unter Anzeigen/Befehlspalette starten.

Voraussetzungen: 
* Installation der Erweiterung "Wokwi"
* diese Zeile in .vscode hinzufügen: `"output":"build"`
* das Beispiel Simple_Servo_Driver.ino compilieren
<br />

## **Erläuterungen der Bibliothek:**

**Erläuterung zur Klasse ServoDriver**

Einbinden der Library:
```Arduino
#include <thk_servo_driver.h>
```
<br />

**Instanziieren eines ServoDriver-Objektes:**

```Arduino
ServoDriver servo(Servo &_servo, int16_t *_p_ang);
```
Bei der Instanziierung eines Servo-Objektes wird eine Referenz auf ein Servoobjekt, ein Vorgabewinkel [Centigrad] übergeben.

```Arduino
ServoDriver servo(Servo &_servo, int16_t *_p_ang, const uint16_t _sampleTime=20, int16_t _K=10);
```
Erläuterung der Parameter: 

* `&servo`: Referenz auf ein Servoobjekt.<br />
* `*_p_ang`: ein Vorgabewinkel [Centigrad].<br />
* `_sampleTime`: optional: Samplezeit [ms].<br />
* `_K`: optional: Faktor für Annäherung an das Ziel [1-10].<br />
<br />

**Erläuterung zu den Funktionen**

`go(a, v)`: Diese Funktion führt die Servobewegung mit der Geschwindigkeit v [Grad/s] auf den Zielwinkel a [Grad] in Schritten durch und muss deshalb im loop() kontinuierlich ausgeführt werden. Jeder Schritt hat die gleiche Dauer.

`set_port(&Serial)`:
Mit dieser Funktion kann eine Referenz auf ein serielles Objekt übergeben werden. Damit können Ausgaben z.B. auf dem seriellen Plotter erzeugt werden.

<br />

# **ServoRamp**

Diese Klasse hat zusätzliche Funktionen für die Servosteuerung und soll die Funktionen der Servo.h-Library etwas erweitern:
 * Ein ServoRamp kann eine Zielstellung in 3 Phasen erreichen: RAMPUP, DRIVE, RAMPDOWN
 * Beim RAMPUP und RAMPDOWN wird die Vorgabegeschwindigkeit halbier, um die Bewegung flüssiger zu machen.
 * Kommt ein neuer Zielwinkel während einer noch andauernden Fahrt, so wird das neue Ziel direkt gültig und das alte Ziel verworfen.
 * go(Winkel) bzw. go(Winkel, Geschwindigkeit) muss dazu im loop kontinuierlich gerufen werden.

## **Anwendung:**

Zur Verwendung siehe die Beispiele:
* Sevo_Ramp.ino
<br />
<br />
  

 **Erläuterung zur Klasse**

Einbinden der Library:
```Arduino
#include <thk_servo_ramp.h>
```
<br />

**Instanziieren eines ServoRamp-Objektes:**

```Arduino
ServoRamp servo(Servo &_servo, int16_t _p_ang_vel);
```
Bei der Instanziierung eines Servo-Objektes wird eine Referenz auf ein Servoobjekt, eine Winkelgeschwindigkeit [Grad/s] übergeben.


**Erläuterung zu den Funktionen**

`go(a)`: Diese Funktion führt die Servobewegung auf den Zielwinkel a [Grad]. Sie muss im loop gerufen werden.

`go(a, v)`: Diese Funktion führt die Servobewegung mit der Geschwindigkeit v [Grad/s] auf den Zielwinkel a [Grad] durch. Sie muss im loop gerufen werden.

`set_port(&Serial)`:
Mit dieser Funktion kann eine Referenz auf ein serielles Objekt übergeben werden. Damit können Ausgaben z.B. auf dem seriellen Plotter erzeugt werden.

`set_angle(Winkel)`:
Mit dieser Funktion wird der Winkelwert der Instanz auf einen gewünschten Wert gesetzt. Das kann beispielsweise beim Start nützlich sein.

<br />